package br.com.ia.oct.mcd.itmass;

import java.util.HashMap;

import javax.ejb.Remote;

import br.com.ia.oct.mcd.scheduler.OCTJob;
import net.sf.json.JSONObject;

@Remote
public interface IncidentOperations extends OCTJob {
	public JSONObject getIncident(String p_incident_no) throws Exception;
	public boolean updateIncident(HashMap<String, String> p_req, HashMap<String, String> p_inc_hpd) throws Exception;
	public String createIncident(HashMap<String, String> p_req, HashMap<String, String> p_inc_hpd, String p_nomeloja, String[] categorization) throws Exception;
	public String getAllCustomAttributesInOneField(String num_incidente);
	public String[] validaCategoriaOperacional(HashMap<String, String> p_inc_hpd);
}
