package br.com.ia.oct.mcd.itmass;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.inteqnet.core.appservices.webservice.beans.xsd.Incident;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.remedy.RemedyBOService;

/**
 * Session Bean implementation class IncidentGetterBean
 */
@Stateless
@Remote(IncidentGetter.class)
@Local(IncidentGetterLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/itmass/incidentgetter")
@EJB(name = "octopus/mcd/itmass/incidentgetter", beanInterface = IncidentGetter.class) 
@OCTJob(uid="4.2.4", nome="IncidentGetterBean", descricao="JOB para leitura da fila ITMASS", jndiLookup="octopus/mcd/itmass/incidentgetter", clazz="", cronTrigger="0 0/30 * 1/1 * ? *", grupo="ITMASS", ativo=true, node="node01")
@OCTComponent(version="$LastChangedRevision$",uid="4.2.4", nome="IncidentGetterBean", descricao="JOB para leitura da fila ITMASS", jndiLookup="MCD/IncidentGetterBean/local", clazz="", cronTrigger="0 0/1 * 1/1 * ? *", grupo="ITMASS", ativo=true)
@TransactionTimeout(value=3300) // 55 minutos
public class IncidentGetterBean implements IncidentGetter, IncidentGetterLocal {
	private static final Logger LOG = Logger.getLogger(IncidentGetterBean.class);
	private static boolean running  = false;

	///@EJB  //(mappedName="octopus/mcd/remedy/boservice")
	@Inject
	private RemedyBOService remedyBO;

	@PostConstruct
	public void inicializa () {
		OCTUtils.setProxyConfig();
	}

	public boolean execute(HashMap<String, Object> params) throws Exception {
		if (running) {
			LOG.warn("Job anterior ainda em execução, saindo...");;
			return true;
		}

		running = true;
		LOG.info("Executando job.");
		boolean result_proc = false;

		try {
			result_proc = this.processAllOpenIncidents();
			
			if (result_proc) {
				LOG.info("Todos os registros foram processados com sucesso.");
			} else {
				LOG.warn("Um ou mais erros ocorreram ao processar os registros.");
			}
		} catch (Exception e) {
			LOG.error("Exception ao obter todos os incidentes abertos. Mensagem: " + e.getMessage() + ".");
		}

		LOG.info("Job executado.");
		running = false;
		return true;
	}

	public Boolean isAlive() {
		return true;
	}

	public boolean processAllOpenIncidents() {
		boolean ret         = false;
		ITMassAPI itMassAPI = new ITMassAPI();
		itMassAPI.init();
		List<Incident> it   = itMassAPI.getIncidentsForCreation();

		for(Incident remd: it){
			HashMap<Integer, Object> mapa = itMassAPI.montaRegistroRemedy(remd);

			try {
				String inc_rmd = remedyBO.createEntry("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", mapa);
				LOG.info("[IncidentGetterBean] - Request gerado no form intermediario: " + inc_rmd);
				itMassAPI.acceptAssignment(remd, inc_rmd);
				LOG.info("[IncidentGetterBean] - Incidente marcado como coletado.: " + inc_rmd);
				ret = true;
			} catch (Exception e) {
				LOG.error("Erro ao criar incident no Remedy: " + e.getMessage());
			}
		}	

		return ret;		
	}
	
	private String buildDebugMsg(Integer p_level, String p_msg) {
		String ret = "";
		String classname = this.getClass().getSimpleName();

		switch (p_level) {
			case 1:
				ret = "[MCD][" + classname + "][WARN]: " + p_msg; 
				break;
			case 2:
				ret = "[MCD][" + classname + "][ERRO]: " + p_msg; 
				break;
			default:
				ret = "[MCD][" + classname + "][INFO]: " + p_msg; 
				break;
		}

		return ret;
	}
	
	private void printDebugMsg(Integer p_level, String p_msg) {
		System.out.println(buildDebugMsg(p_level, p_msg));
	}
}
