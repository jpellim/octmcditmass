package br.com.ia.oct.mcd.itmass;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

import com.inteqnet.components.webservice.beans.xsd.Bean;
import com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse;
import com.inteqnet.core.appservices.webservice.beans.xsd.Credentials;
import com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings;
import com.inteqnet.core.appservices.webservice.beans.xsd.Incident;
import com.inteqnet.core.appservices.webservice.wrappers.IncidentLocator;
import com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.entity.McDMappingTipoMap;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.remedy.CategorizationService;
import br.com.ia.oct.mcd.remedy.RemedyBOService;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@Stateless
//@Remote(IncidentOperations.class)
//@Local(IncidentOperationsLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/itmass/incidentops")
//@EJB(name = "java:global/octopus/mcd/itmass/incidentops", beanInterface = IncidentOperations.class)
@EJB(name = "java:global/OCT-McD/OCT-MCD-ITMASS/IncidentOperationsBean", beanInterface = IncidentOperations.class)
@OCTJob(uid="4.2.2", nome="IncidentOperationsBean", descricao="Wrapper de operações de incidente ITMASS", jndiLookup="octopus/mcd/itmass/incidentops", clazz="", cronTrigger="0 0/30 * 1/1 * ? *", grupo="ITMASS", ativo=false, node="node01")
@OCTComponent(version="$LastChangedRevision$",uid="4.2.2", nome="IncidentOperationsBean", descricao="Wrapper de operações de incidente ITMASS", jndiLookup="MCD/IncidentOperationsBean/local", clazz="", cronTrigger="0 0/1 * 1/1 * ? *", grupo="ITMASS", ativo=false)
//@TransactionTimeout(value=3300) // 55 minutos
public class IncidentOperationsBean implements IncidentOperations, IncidentOperationsLocal {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(IncidentGetterBean.class);
	private static boolean running  = false;
	
	//@EJB //(mappedName="java:jboss/octopus/mcd/remedy/boservice")
	@Inject
	private RemedyBOService remedyBO;

//	@EJB // (mappedName="java:jboss/octopus/mcd/remedy/categorizationservice")	
	@Inject
	private CategorizationService categsrv;

	
	@PostConstruct
	public void inicializa () {
		OCTUtils.setProxyConfig();
	}

	public boolean execute(HashMap<String, Object> params) throws Exception {
		if (running) {
			LOG.info("Job anterior ainda em execução, saindo...");
			return true;
		}

		running = true;
		LOG.info("Job executado, mas este não é planejado para executar periodicamente.");
		running = false;
		return true;
	}

	public Boolean isAlive() {
		return true;
	}
	public JSONObject getIncident(String p_incident_no) throws Exception {
		Credentials credentials           = new Credentials();
		ExtendedSettings extendedSettings = new ExtendedSettings();
		IncidentLocator incidentLocator   = new IncidentLocator();
		IncidentPortType incidentPortType = null;
		DefaultServiceResponse dsResp     = null;
		String resp                       = null;
		JSONObject ret                    = null;
		JSONArray jsonArray               = null;
		int jaSize                        = 0;

		credentials.setUserName(Configuration.getConfig("mcd.itmass.credentials.username"));
		credentials.setUserPassword(Configuration.getConfig("mcd.itmass.credentials.password"));
		extendedSettings.setResponseFormat("JSON");

		System.out.println("User: " + credentials.getUserName() + " Password: " +credentials.getUserPassword());
		System.out.println("Format: " +extendedSettings.getResponseFormat());
		LOG.info("[IncidentOperationsBean] Inicio do getIncident na fujitsu");
		
		try {
			incidentPortType = incidentLocator.getIncidentHttpSoap11Endpoint();

			try {
				dsResp = incidentPortType.getIncident(credentials, extendedSettings, p_incident_no);
				
				try {
					resp      = dsResp.getResponseText();
					jsonArray = (JSONArray) JSONSerializer.toJSON(resp);
				} catch (Exception e) {
					LOG.error("Exception durante leitura do incidente MCD#" + p_incident_no + ". Mensagem: " + e.getMessage());
				}

				if (jsonArray != null && jsonArray.size() > 0) {
					jaSize = jsonArray.size();

					for (int i = 0; i < jaSize; i++) {
						try {
							ret = jsonArray.getJSONObject(i);
							return ret;
						} catch (JSONException e) {
							LOG.error("Erro ao converter JSON ao processar o incidente MCD#" + p_incident_no + ". Mensagem: " + e.getMessage());
							e.printStackTrace();
						} catch (Exception e) {
							LOG.error("Erro desconhecido processar incidente MCD#" + p_incident_no + ". Mensagem: " + e.getMessage());
							e.printStackTrace();
						}
					}
				} else {
					LOG.warn("Resposta nula ou vazia durante obtenção dos incidentes abertos.");
				}

			} catch (RemoteException e1) {
				LOG.error("Exception durante execução da operação getIncident do webservice. Mensagem: " + e1.getMessage());
				e1.printStackTrace();
			}

		} catch (ServiceException e2) {
			LOG.error("Exception ao instanciar webservice. Mensagem: " + e2.getMessage());
		}

		LOG.info("[IncidentOperationsBean] Fim do getIncident: "+p_incident_no);
		return ret;
	}
	
	public Incident getIncident2(String p_incident_no) throws Exception {
		Credentials credentials           = new Credentials();
		ExtendedSettings extendedSettings = new ExtendedSettings();
		IncidentLocator incidentLocator   = new IncidentLocator();
		IncidentPortType incidentPortType = null;
		DefaultServiceResponse dsResp     = null;
		Bean bean 					  = null;
		
		credentials.setUserName(Configuration.getConfig("mcd.itmass.credentials.username"));
		credentials.setUserPassword(Configuration.getConfig("mcd.itmass.credentials.password"));
		extendedSettings.setResponseFormat("BEAN");

		try {
			incidentPortType = incidentLocator.getIncidentHttpSoap11Endpoint();

			try {
				dsResp = incidentPortType.getIncident(credentials, extendedSettings, p_incident_no);
				try {
					bean   = dsResp.getResponseBean();
				} catch (Exception e) {
					LOG.error("Exception durante leitura do incidente MCD#" + p_incident_no + ". Mensagem: " + e.getMessage());
				}
			} catch (RemoteException e1) {
				LOG.error("Exception durante execução da operação getIncident do webservice. Mensagem: " + e1.getMessage());
				e1.printStackTrace();
			}

		} catch (ServiceException e2) {
			LOG.error("Exception ao instanciar webservice. Mensagem: " + e2.getMessage());
		}
		return (Incident) bean;
	}
		
	public boolean updateIncident(HashMap<String, String> p_req, HashMap<String, String> p_inc_hpd) throws Exception {
		Credentials credentials           = new Credentials();
		ExtendedSettings extendedSettings = new ExtendedSettings();
		IncidentLocator incidentLocator   = new IncidentLocator();
		IncidentPortType incidentPortType = null;
		DefaultServiceResponse dsResp     = null;
		boolean ret                       = false;
		Incident inc                      = new Incident();
		JSONObject objMcD                 = null;
		String inc_mcd                    = p_req.get("VCI_INC_USRSRVREST_INCORIG_ID");
//		String inc_tlf                    = p_req.get("VCI_INC_USRSRVREST_INC_REMEDY");

		credentials.setUserName(Configuration.getConfig("mcd.itmass.credentials.username"));
		credentials.setUserPassword(Configuration.getConfig("mcd.itmass.credentials.password"));
		extendedSettings.setResponseFormat("JSON");
		objMcD = this.getIncident(inc_mcd);

		//Basic attrs
		if (objMcD != null) {
			inc.setRow_id(objMcD.getInt("Row ID"));
			inc.setTicket_status("Active");
			inc.setTicket_reason_code("None");
			String desc = "";
			
			if(p_inc_hpd.get("Detailed Decription") != null){
				desc = OCTUtils.cleanString(p_inc_hpd.get("Detailed Decription"));
				inc.setDescription_long(desc);
			}
			inc.setTicket_source("NOC");
			
			if (p_inc_hpd.get("Resolution Category") != null && p_inc_hpd.get("Resolution Category").length() > 0) {
				inc.setResolution(p_inc_hpd.get("Resolution Category") + " / " + p_inc_hpd.get("Resolution Category Tier 2") + " / " + p_inc_hpd.get("Resolution Category Tier 3"));
			}
			
			try {
				LOG.info("[IncidentOperationsBean] Vai executar o getIncidentHttpSoap11Endpoint");
				incidentPortType      = incidentLocator.getIncidentHttpSoap11Endpoint();
				LOG.info("[IncidentOperationsBean] Executou o getIncidentHttpSoap11Endpoint");
	
				try {
					LOG.info("[IncidentOperationsBean] Vai executar Update na fujitsu");
					dsResp          = incidentPortType.updateIncident(credentials, extendedSettings, inc);
					LOG.info("[IncidentOperationsBean] Executou Update");
					String[] errors = dsResp.getErrors();
					
					if (dsResp.getResponseStatus().equalsIgnoreCase("OK")) {
						ret = true;
						
					} else {
						for (String error : errors) {
							LOG.warn(error);
							
						}}
	
				} catch (RemoteException e1) {
					LOG.error("Exception durante execução da operação updateIncident do webservice. Mensagem: " + e1.getMessage());
					// e1.printStackTrace();
				}
	
			} catch (ServiceException e2) {
				LOG.error("Exception ao instanciar webservice. Mensagem: " + e2.getMessage());
				// e2.printStackTrace();
			}
		}

		return ret;
	}

	public String createIncident(HashMap<String, String> p_req, HashMap<String, String> p_inc_hpd, String p_nomeloja, String[] categorization) throws Exception {
		String ret           = null;
		ITMassAPI itmass_api = new ITMassAPI();
		itmass_api.init();

		LOG.info("[createIncident] Inicio CreateIncident");
		
		try{
			
			
			Incident incident= new Incident();
			incident.setDescription_long(OCTUtils.cleanString(p_inc_hpd.get("Detailed Decription")));
			incident.setTicket_priority(Configuration.getConfig("mcd.100.1.1.TicketPriority"));
			incident.setTicket_impact(Configuration.getConfig("mcd.100.1.1.impact.baixo"));
			incident.setTicket_urgency(Configuration.getConfig("mcd.100.1.1.impact.urgency"));
			
			//Ambiente fujitsu Produção
			if("prod".equals(Configuration.getConfig("mcd.fujitsu.ambiente"))){
			
			incident.setRequester_name(p_nomeloja);
			}	
			//Ambiente fujitsu Homologação
			else {
			incident.setRequester_name(Configuration.getConfig("mcd.100.1.1.RequesterName"));
			
			}
			
			incident.setTicket_status(Configuration.getConfig("mcd.100.1.1.TicketStatus"));
			incident.setTicket_description(OCTUtils.cleanString("Case Telefônica:"+p_req.get("VCI_INC_USRSRVREST_INC_ID") +"\n"+ p_inc_hpd.get("Description")));
			incident.setAssigned_group_id(Integer.parseInt((Configuration.getConfig("mcd.100.1.1.AssignedGroupId"))));
			incident.setAssigned_contact_id(Integer.parseInt((Configuration.getConfig("mcd.100.1.1.AssignedContactId"))));
			incident.setTicket_phase(Configuration.getConfig("mcd.100.1.1.TicketPhase"));
			incident.setTicket_reason_code(Configuration.getConfig("mcd.100.1.1.TicketReasonCode"));
			incident.setTicket_source(Configuration.getConfig("mcd.100.1.1.TicketSource"));
			incident.setCcti_class(Configuration.getConfig("mcd.100.1.1.CctiClass"));
			

			incident.setCcti_category(categorization[0]);
			incident.setCcti_item(categorization[1]);
			incident.setCcti_type(categorization[2]);
			
			LOG.info("[IncidentOperationsBean] Vai executar reportIncident na fujitsu");
			ret = itmass_api.reportIncident(incident);
			LOG.info("[IncidentOperationsBean] Executou ReportIncident: "+ret);		
			
			

		}catch (Exception e) {
			ret = null;
			LOG.error("Erro no método reportIncident(). Mensagem: " + e.getMessage());
		}

		return ret;
	}


	public String[] validaCategoriaOperacional(HashMap<String, String> p_inc_hpd) {
		
		String[] returnData = null;

		String[] categorization = this.getCategoriaOperacional(p_inc_hpd.get("Categorization Tier 1"), 
				p_inc_hpd.get("Categorization Tier 2"), p_inc_hpd.get("Categorization Tier 3"), "");
		
		if (categorization != null && categorization.length > 0) {
			if (categorization[4] != null && !categorization[4].equalsIgnoreCase("N")) {				
				returnData = categorization;
				
			} else {				
				LOG.warn("Categorizacao operacional não mapeada para o incidente TLF#" + p_inc_hpd.get("Incident Number"));
			}
		} else {
			LOG.warn("Impossivel determinar a categorizacao operacional para o incidente TLF#" + p_inc_hpd.get("Incident Number"));
		}
		
		return returnData;
	}
	
//	public List<HashMap<String, String>> getCategorization(String tier1, String tier2, String tier3){
//		String formMap = "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS";
//		String query = "'Tier 1'  = \""+tier1+ "\" AND 'Tier 2'  = \""+tier2+ "\" AND 'Tier 3'  = \""+tier3+"\"" ;
//		List<HashMap<String, String>> ret = null;	
//		List<HashMap<String, String>> queryEntry= remedyBO.frmQuery(formMap,query);
//		
//		if(queryEntry.size()== 0){
//			ret = getCategorizationDefault();
//			return ret;
//		}
//		return queryEntry;
//	}
	
	private String[] getCategoriaOperacional(String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
//		String[] ret = this.getMap("Categorizacao", p_de_1, p_de_2, p_de_3, p_de_4);
		LOG.info("Buscando Mapeamento de Categoria Operacional");
		String[] ret = categsrv.getMap("", String.valueOf(McDMappingTipoMap.Cat.ordinal()), "", p_de_1, p_de_2, p_de_3, p_de_4);
		
		if (ret != null && ret.length > 0) {
			return ret;
		}
		
		return null;
	}

	private String[] getCategoriaDeResolucao(String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
//		String[] ret = this.getMap("Cat Resolucao", p_de_1, p_de_2, p_de_3, p_de_4);
		String[] ret = categsrv.getMap("", String.valueOf(McDMappingTipoMap.Cat_Res.ordinal()), "", p_de_1, p_de_2, p_de_3, p_de_4);
		return ret;
	}

	private String[] getImpacto(String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
//		String[] ret = this.getMap("Impact", p_de_1, p_de_2, p_de_3, p_de_4);
		String[] ret = categsrv.getMap("", String.valueOf(McDMappingTipoMap.Impacto.ordinal()), "", p_de_1, p_de_2, p_de_3, p_de_4);
		return ret;
	}

	private String[] getUrgencia(String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
//		String[] ret = this.getMap("Urgency", p_de_1, p_de_2, p_de_3, p_de_4);
		String[] ret = categsrv.getMap("", String.valueOf(McDMappingTipoMap.Urgencia.ordinal()), "", p_de_1, p_de_2, p_de_3, p_de_4);
		return ret;
	}

//	private String[] getMap(String p_map, String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
//		String formMap = "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS";
//		String[] ret   = new String[5];
//		String qryB    = "'536870925' = \"" + p_map + "\" AND '7' = 0";
//		String qryC    = "'536870925' = \"" + p_map + "\" AND '7' = 0 AND '536870913' = \"Default\"";
//		List<HashMap<String, String>> recs = null;
//
//		if (p_de_1 != null && p_de_1.trim().length() > 0) {
//			qryB += " AND '536870914' = \"" + p_de_1.trim() + "\"";  
//		}
//
//		if (p_de_2 != null && p_de_2.trim().length() > 0) {
//			qryB += " AND '536870915' = \"" + p_de_2.trim() + "\"";  
//		}
//
//		if (p_de_3 != null && p_de_3.trim().length() > 0) {
//			qryB += " AND '536870916' = \"" + p_de_3.trim() + "\"";  
//		}
//
//		if (p_de_4 != null && p_de_4.trim().length() > 0) {
//			qryB += " AND '536870920' = \"" + p_de_4.trim() + "\"";  
//		}
//
//		LOG.info("Buscando mapeamento (" + qryB + ")...");
//		recs = remedyBO.frmQuery(formMap, qryB);
//
//		if (recs != null && recs.size() > 0) {
//			ret[0] = recs.get(0).get("Categoria");
//			ret[1] = recs.get(0).get("ccti_item");
//			ret[2] = recs.get(0).get("ccti_type");
//			ret[3] = recs.get(0).get("Last_Name2");
//		} else {
//			LOG.warn("Mapeamento requerido nao encontrado, buscando mapeamento default (" + p_map + ")");
//			LOG.info("Buscando mapeamento (" + qryC + ")...");
//			recs = remedyBO.frmQuery(formMap, qryC);
//
//			if (recs != null && recs.size() > 0) {
//				LOG.info("Mapeamento default encontrado (" + p_map + ")!");
//				ret[0] = recs.get(0).get("Categoria");
//				ret[1] = recs.get(0).get("ccti_item");
//				ret[2] = recs.get(0).get("ccti_type");
//				ret[3] = recs.get(0).get("Last_Name2");
//			} else {
//				LOG.warn("Mapeamento default não encontrado (" + p_map + ")!");
//				ret[0] = "N/A#1";
//				ret[1] = "N/A#2";
//				ret[2] = "N/A#3";
//				ret[3] = "N/A#4";
//			}
//		}
//
//		return ret;
//	}

//	private List<HashMap<String, String>> getCategorizationDefault() {
//		String formMap = "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS";
//		String query = "'Request ID'  = \"1\"" ;
//		
//		List<HashMap<String, String>> queryEntry= remedyBO.frmQuery(formMap,query);
//		
//		return queryEntry;
//	}

	public String getAllCustomAttributesInOneField(String num_incidente) {
		   String att = "";
		try {
			Incident incident = this.getIncident2(num_incidente); 
			att = incident.getDescription_long();
			} catch (Exception e) {
				LOG.error("Erro ao retornar campo Notes do Incident: " +num_incidente +" Mensagem: " + e.getMessage());
				return null;
			}
		return att;
	}
}
