package br.com.ia.oct.mcd.itmass;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

import com.inteqnet.components.webservice.beans.xsd.Bean;
import com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse;
import com.inteqnet.core.appservices.webservice.beans.xsd.Credentials;
import com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute;
import com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings;
import com.inteqnet.core.appservices.webservice.beans.xsd.Incident;
import com.inteqnet.core.appservices.webservice.beans.xsd.Worklog;
import com.inteqnet.core.appservices.webservice.wrappers.IncidentLocator;
import com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType;

import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class ITMassAPI {
	private static final Logger LOG = Logger.getLogger(ITMassAPI.class);
	private Credentials credentials;
	private ExtendedSettings extendedSettings;
	private IncidentLocator incidentLocator;
	private IncidentPortType incidentPortType;

	public void init() {

		// instancia o stub
		credentials = new Credentials();
		extendedSettings = new ExtendedSettings();
		incidentLocator = new IncidentLocator();
		credentials.setUserName("wsTelefonica@mcd");
		credentials.setUserPassword("fujitsu123");
		extendedSettings.setResponseFormat("JSON");
		OCTUtils.setProxyConfig();

		try {
			incidentPortType = incidentLocator.getIncidentHttpSoap11Endpoint();
		} catch (ServiceException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
	}

	/* LISTA TODOS INCIDENTE */
	public List<Incident> getAllOpenIncidents() {

		List<Incident> ret = new ArrayList<Incident>();
		JSONArray jsonArray = null;
		JSONObject jsonObj = null;
		String jsonResp = null;
		DefaultServiceResponse dsResp = null;
		String error = "";

		try {
			dsResp = incidentPortType.listIncidents(credentials, extendedSettings, "");
			jsonResp = dsResp.getResponseText();
			jsonArray = (JSONArray) JSONSerializer.toJSON(jsonResp);
			
			//LOG.error("[getAllOpenIncidents] JSON ARRAY:" + jsonArray.toString() +"\n"+ "JSON_RESP: " + dsResp.getErrors(0).toString());
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("[getAllOpenIncidents] Error conection response. Message: "+ e.getMessage());

		}

		if (jsonArray != null && jsonArray.size() > 0) {
			for (int i = 0; i < jsonArray.size(); i++) {
				try {
					jsonObj = jsonArray.getJSONObject(i);
					Incident tmpInc = new Incident();
					tmpInc.setAssigned_to_group_name(jsonObj.getString("Assigned Group"));
					tmpInc.setTicket_identifier(jsonObj.getString("Case#"));
					tmpInc.setRow_id(jsonObj.getInt("Row ID"));
					tmpInc.setAssigned_to_individual_name(jsonObj.getString("Assigned Individual"));
					tmpInc.setCreated_date(jsonObj.getString("Created Date"));
					tmpInc.setDescription_long(jsonObj.getString("Description"));
					tmpInc.setModified_date(jsonObj.getString("Modified Date"));
					tmpInc.setTicket_priority(jsonObj.getString("Priority"));
					tmpInc.setTicket_reason_code(jsonObj.getString("Reason Code"));
					tmpInc.setRequester_name(jsonObj.getString("Requester"));
					tmpInc.setRequested_for_name(jsonObj.getString("Requester Organization"));
					tmpInc.setTicket_status(jsonObj.getString("Status"));
					tmpInc.setCcti_type(jsonObj.getString("Type"));
					ret.add(tmpInc);
				} catch (JSONException e1) {
					LOG.error("JSON Error. Message: " + e1.getMessage());
				} catch (Exception e2) {
					error = dsResp.getErrors(0);
					LOG.error("[getAllOpenIncidents] Generic error: " + error);
					LOG.error("[getAllOpenIncidents] Generic error. Message: " + e2.getMessage());
				}
			}
		}

		return ret;
	}

	/* LISTA TODOS INCIDENTE */
	public List<JSONObject> getAllOpenIncidentsJSON() {

		List<JSONObject> ret = new ArrayList<JSONObject>();
		JSONArray jsonArray = null;
		JSONObject jsonObj = null;
		String jsonResp = null;
		DefaultServiceResponse dsResp = null;
		String error = "";

		try {
			dsResp = incidentPortType.listIncidents(credentials, extendedSettings, "");
			jsonResp = dsResp.getResponseText();
			jsonArray = (JSONArray) JSONSerializer.toJSON(jsonResp);

		} catch (Exception e) {
			e.printStackTrace();

		}

		if (jsonArray != null && jsonArray.size() > 0) {
			for (int i = 0; i < jsonArray.size(); i++) {
				try {
					jsonObj = jsonArray.getJSONObject(i);
					ret.add(jsonObj);
				} catch (JSONException e1) {
					System.out.println("JSON Error. Message: " + e1.getMessage());
				} catch (Exception e2) {
					error = dsResp.getErrors(0);
					System.out.println("Generic error: " + error);
					System.out.println("Generic error. Message: " + e2.getMessage());
				}
			}
		}

		return ret;
	}

	public HashMap<Integer, Object> montaRegistroRemedy(Incident it) {
		HashMap<Integer, Object> ret = new HashMap<Integer, Object>();

		/*
		 * { "Assigned Group": "MCD Telefonica", "Assigned Individual": "",
		 * "Case#": "300-318123", "Created Date": "09/01/2015 08:31:22 AM",
		 * "Description": "[Teste] Integração Telefônica", "Modified Date":
		 * "09/01/2015 09:52:17 AM", "Priority": "High", "Reason Code": "None",
		 * "Requester": "test, ssu", "Requester Organization":
		 * "Arcos Dourados Comércio de Alimentos Ltda",
		 * "Requester Organization Hierarchical Path":
		 * "Arcos Dourados Comércio de Alimentos Ltda >> [SIA] SHOPPING IGUATEMI ALPHAVILLE"
		 * , "Row ID": "410514", "Status": "Escalated", "Type": "INC" }
		 */

		try {
			System.out.println("Montando registro Remedy...");
			String uuid = OCTUtils.generateUUID();
			ret.put(8, "INT_MCDONALDS"); // Short Description
			ret.put(536870913, "SERVIÇO VCI"); // VCI_INC_USRSRVREST_PRODSERV
			ret.put(536870914, "BRASIL"); // VCI_INC_USRSRVREST_IDREGIONAL
			ret.put(536870915, "VCI-INCIDENTES"); // VCI_INC_USRSRVREST_ID_INTEGRACAO
			ret.put(536870916, "USER SERVICE RESTORATION"); // VCI_INC_USRSRVREST_INCTIPO

			// ret.put(536870917, p_jsonItem.getString("Requester
			// Organization")); // VCI_INC_USRSRVREST_KEYCLIENTE
			ret.put(536870917, it.getPerson1_lvl1_org_name());// VCI_INC_USRSRVREST_KEYCLIENTE

			ret.put(536870918, "ARCOS DOURADOS - DC"); // VCI_INC_USRSRVREST_NOMECLIENTE
			ret.put(536870919, "MC DONALDS"); // VCI_INC_USRSRVREST_ABREV
			ret.put(536870920, "MC DONALDS-INCIDENTES"); // VCI_INC_USRSRVREST_INTEGRACAOCLIENTE
			ret.put(536870921, uuid); // VCI_INC_USRSRVREST_MSGBUS_ID
			ret.put(536870922, "GESTÃO DE INCIDENTES"); // VCI_INC_USRSRVREST_OPER
			ret.put(536870923, "REQ INCLUSÃO"); // VCI_INC_USRSRVREST_SUBFUNC

			// ret.put(536870924, p_jsonItem.getString("Row ID")); //
			// VCI_INC_USRSRVREST_REQ_ID
			// ret.put(536870925, p_jsonItem.getString("Case#")); //
			// VCI_INC_USRSRVREST_INC_ID
			// ret.put(536870929, p_jsonItem.getString("Reason Code")); //
			// VCI_INC_USRSRVREST_REASONCODE
			ret.put(536870924, it.getRow_id()); // VCI_INC_USRSRVREST_REQ_ID
			// ret.put(536870925, it.getTicket_identifier()); //
			// VCI_INC_USRSRVREST_INC_ID
			ret.put(536870929, it.getTicket_reason_code()); // VCI_INC_USRSRVREST_REASONCODE

			ret.put(536870932, "NOVO"); // VCI_INC_USRSRVREST_STATUS
			ret.put(536870933, uuid); // VCI_INC_USRSRVREST_HASH

			// ret.put(536870948, p_jsonItem.getString("Case#")); //
			// VCI_INC_USRSRVREST_INCORIG_ID
			ret.put(536870948, it.getTicket_identifier()); // VCI_INC_USRSRVREST_INCORIG_ID

			ret.put(536870954, it.toString()); // VCI_INC_USRSRVREST_RAW_XML

		} catch (JSONException e) {
			System.out.println("Exception ocorreu durante montagem do registro Remedy. Mensagem: " + e.getMessage());
		}

		return ret;
	}

	/* RETORNA UM INCIDENTE ESPECÍFICO */
	public Incident getIncident(String ticketIdentifier) {
		DefaultServiceResponse dsResp = null;
		Bean bean = null;
		String error = "";

		try {
			dsResp = incidentPortType.getIncident(credentials, new ExtendedSettings("BEAN"), ticketIdentifier);
			bean = dsResp.getResponseBean();

			
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error getIncident(). Message: " + e.getMessage());
		}
		return (Incident) bean;
	}

	/* LISTA TODOS WORKLOGS */
	public List<Worklog> listWorklogs(String ticketIdentifier) {

		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		JSONObject jsonObj = null;
		List<Worklog> worklogs = new ArrayList<Worklog>();
		String error = "";

		try {
			dsResp = incidentPortType.listWorklogs(credentials, extendedSettings, ticketIdentifier, "");
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jsonArray != null && jsonArray.size() > 0) {

			for (int i = 0; i < jsonArray.size(); i++) {

				try {

					jsonObj = jsonArray.getJSONObject(i);
					Worklog worklog = new Worklog();
					worklog.setWork_created_by(jsonObj.getString("Created By"));
					worklog.setWork_created_date(jsonObj.getString("Created On"));
					worklog.setRow_id(jsonObj.getInt("Row ID"));
					worklog.setWork_description(jsonObj.getString("Worklog Description"));
					worklogs.add(worklog);
				} catch (JSONException e1) {
					System.out.println("[listWorklogs] JSON Error. Message: " + e1.getMessage());
				} catch (Exception e2) {
					error = dsResp.getErrors(0);
					System.out.println("[listWorklogs] Generic error: " + error);
					System.out.println("[listWorklogs] Generic error. Message: " + e2.getMessage());
				}
			}
		}
		return worklogs;
	}

	/**
	 * Cria um WorkLog na fujitsu
	 * @param worklog
	 * @return Número do worklog criado
	 */
	public int addWorklog(Worklog worklog) {

		DefaultServiceResponse dsResp = null;
		String error = "";
		JSONArray jsonArray = null;
		int result = 0;
		JSONObject jsonObj = null;
		
		try {
			LOG.info("[addWorklog] Criando um worklog na fujitsu. MCD#: " + worklog.getTicket_identifier());
			
			dsResp = incidentPortType.addWorklog(credentials, extendedSettings, worklog);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());

			if (jsonArray != null && jsonArray.size() > 0) {

				jsonObj = jsonArray.getJSONObject(0);
				result = jsonObj.getInt("row_id");
			}
		} catch (JSONException e1) {
			LOG.info("[addWorklog] JSON Error. Message: " + e1.getMessage());
		} catch (Exception e2) {
			error = dsResp.getErrors(0);
			LOG.info("[addWorklog] Generic error: " + error);
			LOG.info("[addWorklog] Error addWorklog(). Message: " + e2.getMessage());
		}
		return result;
	}

	/* RELATA UM ANEXO */
	public boolean relateAttachment(String ticketIdentifier, int attachmentId) {

		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.relateAttachment(credentials, extendedSettings, ticketIdentifier, attachmentId);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error relateAttachment(). Message: " + e.getMessage());
		}
		return result;
	}

	/* RELATA A CONFIGURAÇAO DE UM ITEM */
	public boolean relateConfigurationItem(String ticketIdentifier, int confID, String confItemID) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.relateConfigurationItem(credentials, extendedSettings, ticketIdentifier, confID,
					confItemID);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			error = dsResp.getErrors(0);

			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}

		} catch (JSONException e1) {
			System.out.println("JSON Error. Message: " + e1.getMessage());
		} catch (Exception e2) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error relateConfigurationItem(). Message: " + e2.getMessage());
		}
		return result;
	}

	/* RELATA UM TICKET */
	public boolean relateTIcket(String parentTicketIdentifier, String relateTicketIdentifier) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.relateTicket(credentials, extendedSettings, parentTicketIdentifier,
					relateTicketIdentifier);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}

		} catch (Exception e2) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error relateTIcket(). Message: " + e2.getMessage());
		}
		return result;
	}

	/* CRIA UM INCIDENTE */
	public String reportIncident(Incident incBean) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		String result = "";
		String error = "";
		JSONObject jsonObj = null;

		try {
			dsResp = incidentPortType.reportIncident(credentials, extendedSettings, incBean);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println(
					"Error reportIncident(). API Message: " + error + ". Exception Message: " + e.getMessage());

		}

		try {
			if (jsonArray != null && jsonArray.size() > 0) {
				jsonObj = jsonArray.getJSONObject(0);
				result = jsonObj.getString("ticket_identifier");
			}
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error reportIncident(). Message: " + e.getMessage());
		}

		return result;
	}

	/* DESRELACIONAR ANEXO */
	public boolean unrelateAttachment(String ticketIdentifier, int attachmentId) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.unrelateAttachment(credentials, extendedSettings, ticketIdentifier, attachmentId);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());

			if (jsonArray != null && jsonArray.size() > 0) {
				result = true;
			}
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error unrelateAttachment(). Message: " + e.getMessage());
		}

		return result;
	}

	/* DESRELACIONAR O ITEM DE CONFIGURAÇÃO */
	public boolean unrelateConfigurationItem(String ticketIdentifier, int configurationItemId,
			String configurationItemIdentifier) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";
		try {
			dsResp = incidentPortType.unrelateConfigurationItem(credentials, extendedSettings, ticketIdentifier,
					configurationItemId, configurationItemIdentifier);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}
		} catch (Exception e2) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error unrelateConfigurationItem(). Message: " + e2.getMessage());
		}
		return result;
	}

	/* DESRELACIONAR UM TICKET */
	public boolean unrelateTicket(String unrelateTicket, String relateTicketIdentifier) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.unrelateTicket(credentials, extendedSettings, unrelateTicket,
					relateTicketIdentifier);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}
		} catch (Exception e2) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error relateConfigurationItem(). Message: " + e2.getMessage());
		}
		return result;
	}

	/* ATUALIZA INCIDENTE */
	public boolean updateIncident(Incident incident) {
		DefaultServiceResponse dsResp = null;
		JSONArray jsonArray = null;
		Boolean result = false;
		String error = "";

		try {
			dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);

			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error updateIncident(). Message: " + e.getMessage());
		}
		return result;
	}

	/* ENVIA NUMERO INCIDENT DO REMEDY PARA A ITMASS */
	public boolean callBackIncidentNumberToRemedy(String inc_mcd, String inc_tlf) {
		int numWork = 0;
		boolean result = false;
		try {
			Worklog worklog = new Worklog();
			worklog.setTicket_identifier(inc_mcd);
			worklog.setWork_description("Número do Incidente criado na Telefônica: "+ inc_tlf);
			
			numWork = this.addWorklog(worklog);

			if (numWork!= 0) {
				result = true;
				LOG.info("[callBackIncidentNumberToRemedy] RETORNO DO WORKLOG: " + numWork);
			}
		} catch (Exception e) {
			LOG.info("[callBackIncidentNumberToRemedy] Ocorreu um problema ao enviar o número do Incidente para Fujitsu. Mensagem: " + e.getMessage());
		}
		return result;
	}

	/*
	 * RETORNA UM WORKLOG ESPECÍFICO NÃO ESTÁ FUNCIONANDO POIS ALTERARAM O WSDL,
	 * WORKLOG TEM UM ENDEREÇO ESPECIFICO
	 */
	public Worklog getWorklog(String worklogId) {

		DefaultServiceResponse dsResp = null;
		Bean bean = null;
		String error = "";

		try {
			// dsResp = incidentPortType.getWorklog(credentials, new
			// ExtendedSettings("BEAN"), worklogId);
			bean = dsResp.getResponseBean();
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("Generic error: " + error);
			System.out.println("Error getIncident(). Message: " + e.getMessage());
		}

		return (Worklog) bean;
	}

	/* FILTRA OS CHAMADOS */
	public List<Incident> getIncidentsForCreation() {

		List<Incident> incidents = getAllOpenIncidents();
		Iterator<Incident> it = incidents.iterator();

		
		
		try {
			while (it.hasNext()) {
				Incident incident = it.next();
	
				//Ambiente fujitsu Produção
				if("prod".equals(Configuration.getConfig("mcd.fujitsu.ambiente"))){
					
					if (incident.getTicket_status().equals("Escalated") && (!incident.getRequester_name().equals(Configuration.getConfig("mcd.100.1.1.RequesterName"))) ){
						continue;
					}
					
				}
				
				else {
				
				//Ambiente fujitsu Homologação
					if (incident.getTicket_status().equals("Escalated") && incident.getRequester_name().equals(Configuration.getConfig("mcd.100.1.1.RequesterName"))) {
						continue;
					}
				
				}
				
				it.remove();
			}

		} catch (Exception e) {
			System.out.println("Error getIncidentsForCreation(). Message: " + e.getMessage());
			return null;
		}
		return incidents;
	}

	/* ACEITAR ATRIBUIÇÃO 
	 * Não esta funcionando devidamente, verificar.
	 * */
	public boolean acceptAssignment(Incident incident, String provideNumber) {

		DefaultServiceResponse dsResp = null;
		String error = "";
		Boolean result = false;
		JSONArray jsonArray = null;

		Incident inc_atualizar = new Incident();
		
		inc_atualizar.setRow_id(incident.getRow_id());
		inc_atualizar.setTicket_status("Active");

		try {

			System.out.println("[acceptAssignment] - Vai atualizar o incident RowId: " + incident.getRow_id()
					+ " para status ativo e com numero do Incident TLF: " + provideNumber);
			 setCustomAttributeValue(incident, "provider number", provideNumber);
			
			 dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());

			if (jsonArray != null && jsonArray.size() > 0) {

				result = true;
			}
		} catch (Exception e) {
			error = dsResp.getErrors(0);
			System.out.println("[acceptAssignment]Generic error: " + error);
			System.out.println("Error acceptAssignment(). Message: " + e.getMessage());
		}
		return result;
	}

	/* ACEITAR ATRIBUIÇÃO COMPLEMENTO */
	public void setCustomAttributeValue(Incident incident, String name, String value) {

		try {
			CustomAttribute[] customAttributes = incident.getCustom_attributes();

			for (CustomAttribute customAttibute : customAttributes) {

				if (customAttibute.getAttribute_name().equals(name)) {
					customAttibute.setAttribute_value(value);
					break;
				}
			}
		} catch (Exception e) {
			LOG.error("[setCustomAttributeValue] Generic error:" + e.getMessage());
		}
	}

	/**
	 * Resolve um chamado na fujitsu
	 * @param inc_mcd Número do chamado na fujitsu
	 * @param p_resolution Descrição da resolução do incidente na Telefônica
	 * @return
	 */
	public boolean resolveTicket(String inc_mcd, String p_resolution) {
		DefaultServiceResponse dsResp = null;
		String error        = "";
		Boolean result      = false;
		JSONArray jsonArray = null;
		String resolution   = "";

		try {
			Incident incident = new Incident();
			Incident incidentMcd = getIncident(inc_mcd);

			LOG.info("Atribuindo RowID...");
			incident.setRow_id(incidentMcd.getRow_id());

			LOG.info("Atribuindo TicketPhase...");
			incident.setTicket_phase("Closure");

			LOG.info("Atribuindo TicketStatus...");
			incident.setTicket_status("Resolved");

			if (p_resolution == null || p_resolution.trim().length() == 0) {
				resolution = "Sem informacoes adicionais.";
			} else {
				/*
				 * Segundo e-mail de 02/05/2016 10:12, Demian Parenti Quirino
				 * <dquirino@br.fujitsu.com> solicitou (com ciência do Sr. Dadário),
				 * solicitou que todos os espaços fossem trocados por "_".
				 * 
				 * Deixamos claro de que esta não é a melhor solução para este
				 * problema.
				 */
				resolution = p_resolution.replace(" ", "_");
			}

			LOG.info("Atribuindo Resolution...");
			incident.setResolution(resolution);

			LOG.info("[resolveTicket] - resolveTicket MCD#" + inc_mcd + ". Comprimento da resolução: " + resolution.length() + ".");
			dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);
			LOG.info("Response: " + dsResp.getResponseText());
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());
			error = dsResp.getErrors(0);
			if (jsonArray != null && jsonArray.size() > 0) {
				result = true;
				LOG.info("[resolveTicket] - resolveTicket MCD#" + inc_mcd + ", INI Resposta McD");
				LOG.info("---------------------------------------------");
				LOG.info(jsonArray.toString());
				LOG.info("---------------------------------------------");
				LOG.info("[resolveTicket] - resolveTicket MCD#" + inc_mcd + ", FIM Resposta McD");
			} else {
				LOG.info("[resolveTicket] - resolveTicket MCD#" + inc_mcd + ": Erro ao gerar objeto JSONArray da resposta McD. Resposta McD:" + dsResp.getResponseText());
			}

		} catch (Exception e) {
			if (!error.isEmpty()) {
				LOG.error("[resolveTicket] Generic error: " + error);
			}
			LOG.error("[resolveTicket] Generic error. Message: " + e.getMessage());
		}
		
		return result;
	}

	/* DEVOLVE POR FALTA DE INFORMAÇÃO */
	public void returnForPedingInformation(Incident incident, String worklogDescription) {

		DefaultServiceResponse dsResp = null;
		String error = null;

		try {
			Worklog worklog = new Worklog();
			worklog.setTicket_identifier(incident.getTicket_identifier());
			worklog.setWork_description(worklogDescription);

			dsResp = incidentPortType.addWorklog(credentials, extendedSettings, worklog);
			error = dsResp.getErrors(0);

			if (!error.isEmpty()) {
				System.out.println("Generic error:" + error);
			}

			incident.setTicket_phase("Initial Diagnosis");
			incident.setTicket_status("Queued");
			incident.setTicket_reason_code("Pending Information");
			incident.setAssigned_to_group_name("MCD N1");

			dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);
			error = dsResp.getErrors(0);

			if (!error.isEmpty()) {
				System.out.println("Generic error:" + error);
			}

		} catch (Exception e) {
			System.out.println("Error returnForPedingInformation(). Message:" + e.getMessage());
		}
	}
	
	public boolean ticketForPending(HashMap<String, String> p_req) {
		boolean ret                   = false;
		DefaultServiceResponse dsResp = null;
		String error                  = "";
		String inc_mcd                 = null;
		JSONArray jsonArray           = null;

		if (p_req == null || p_req.size() == 0) {
			LOG.info("[ticketForPending] Um erro ocorreu ao passar o incidente para o status 'Pendente' (objeto inválido).");
			return ret;
		} else {
			inc_mcd = p_req.get("VCI_INC_USRSRVREST_INCORIG_ID");
		}
		
		
		try { 
			
			Incident incident    = new Incident();
			Incident incidentMcd = getIncident(inc_mcd);
			LOG.info("[ticketForPending] Atribuindo RowID, Phase e Status...");
			LOG.info("[ticketForPending] Status Reason: " + p_req.get("VCI_INC_USRSRVREST_STATUS_REASON"));
					
			incident.setRow_id(incidentMcd.getRow_id());
					
			// Third Party Vendor Action Reqd
			if(p_req.get("VCI_INC_USRSRVREST_STATUS_REASON").equals("7000")){
				
				String worklogDesc = p_req.get("VCI_INC_USRSRVREST_STATUS_REASON") + " " +p_req.get("VCI_INC_USRSRVREST_INC_ID");
				this.returnForPedingInformation(incidentMcd,worklogDesc);
				return true;
				
			}
			
			// Support Contact Hold 
			if(p_req.get("VCI_INC_USRSRVREST_STATUS_REASON").equals("6000")){
				incident.setTicket_reason_code("Field Support");
				incident.setAssigned_to_group_name("MCD N1");
			}else{
				incident.setTicket_reason_code("None");
			}
		
			
			incident.setTicket_phase("Initial Diagnosis");
			incident.setTicket_status("Pending");
			
			Worklog worklog = new Worklog();
			worklog.setTicket_identifier(inc_mcd);
			worklog.setWork_description("Chamado enviado para o Field.");
			this.addWorklog(worklog);
			
			LOG.info("[ticketForPending] - Passar para Pendente MCD#" + inc_mcd + ".");
			dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());

			if (jsonArray != null && jsonArray.size() > 0) {
				ret = true;
				LOG.info("[ticketForPending] MCD#" + inc_mcd + ", INI Resposta McD");
				LOG.info("---------------------------------------------");
				LOG.info(jsonArray.toString());
				LOG.info("---------------------------------------------");
				LOG.info("[ticketForPending] MCD#" + inc_mcd + ", FIM Resposta McD");
			} else {
				LOG.info("[ticketForPending] - ticketForPending MCD#" + inc_mcd + ": Erro ao gerar objeto JSONArray da resposta McD. Resposta McD:" + dsResp.getResponseText());
			}
		} catch (Exception e) {
			LOG.error("[ticketForPending] Uma exceção desconhecida ocorreu. Detalhes:" + e.getMessage());

			if (dsResp != null && dsResp.getErrors() != null && dsResp.getErrors().length > 0) {
				error = dsResp.getErrors(0);
				LOG.error("[ticketForPending] Generic error: " + error);
			}
			LOG.error("[ticketForPending] Error. Detalhes:" + e.getMessage());
		}
		return ret;
	}
	
	/**
	 * Cancela o chamado na fujitsu
	 * @param p_req Parâmetros do incidente
	 * @return Validação do tipo boolean
	 * @throws RemoteException 
	 */
	public boolean ticketForCanceled(HashMap<String, String> p_req) throws RemoteException {
		boolean ret                   = false;
		DefaultServiceResponse dsResp = null;
		String error                  = "";
		String inc_mcd                 = null;
		JSONArray jsonArray           = null;

		if (p_req == null || p_req.size() == 0) {
			LOG.error("[ticketForCanceled] Um erro ocorreu ao passar o incidente para o status 'Cancelado' (objeto inválido).");
			return ret;
		} else {
			inc_mcd = p_req.get("VCI_INC_USRSRVREST_INCORIG_ID");
		}

		try { 
			
			Incident incident    = new Incident();
			Incident incidentMcd = getIncident(inc_mcd);
			LOG.info("[ticketForCanceled] Atribuindo RowID, Phase, Status e Reason Code...");
			
			incident.setRow_id(incidentMcd.getRow_id());
			incident.setTicket_status("Closed");
			incident.setTicket_reason_code("Canceled");
			incident.setTicket_phase("Closure");			
			
			Worklog worklog = new Worklog();
			worklog.setTicket_identifier(inc_mcd);
			worklog.setWork_description("Chamado cancelado pela telefônica.");
			this.addWorklog(worklog);
			
			LOG.info("[ticketForCanceled] - Passando para Cancelado MCD#" + inc_mcd + ".");
			dsResp = incidentPortType.updateIncident(credentials, extendedSettings, incident);
			jsonArray = (JSONArray) JSONSerializer.toJSON(dsResp.getResponseText());

			if (jsonArray != null && jsonArray.size() > 0) {
				ret = true;
				LOG.info("[ticketForCanceled] MCD#" + inc_mcd + ", INI Resposta McD");
				LOG.info("---------------------------------------------");
				LOG.info(jsonArray.toString());
				LOG.info("---------------------------------------------");
				LOG.info("[ticketForCanceled] MCD#" + inc_mcd + ", FIM Resposta McD");
			} else {
				LOG.info("[ticketForCanceled] MCD#" + inc_mcd + ": Erro ao gerar objeto JSONArray da resposta McD. Resposta McD:" + dsResp.getResponseText());
			}
		} catch (JSONException e) {
			LOG.error("[ticketForCanceled] Uma exceção desconhecida ocorreu. Detalhes:" + e.getMessage());

			if (dsResp != null && dsResp.getErrors() != null && dsResp.getErrors().length > 0) {
				error = dsResp.getErrors(0);
				LOG.error("[ticketForCanceled] Generic error: " + error);
			}
			LOG.error("[ticketForCanceled] Error. Detalhes:" + e.getMessage());
		}
		return ret;
	}
}
