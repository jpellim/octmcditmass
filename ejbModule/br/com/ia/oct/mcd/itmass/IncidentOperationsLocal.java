package br.com.ia.oct.mcd.itmass;

import java.util.HashMap;

import javax.ejb.Local;

import br.com.ia.oct.mcd.common.monitoring.OCTComponent;
import br.com.ia.oct.mcd.scheduler.OCTJob;
import net.sf.json.JSONObject;

@Local
public interface IncidentOperationsLocal extends OCTJob, OCTComponent {
	public JSONObject getIncident(String p_incident_no) throws Exception;
	public boolean updateIncident(HashMap<String, String> p_req, HashMap<String, String> p_inc_hpd) throws Exception;
}
