package br.com.ia.oct.mcd.itmass.register;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import br.com.ia.oct.mcd.itmass.IncidentGetterBean;
import br.com.ia.oct.mcd.mgmt.register.MgmtRegister;
import br.com.ia.oct.mcd.mgmt.register.OCTRegister;

/**
 * Session Bean implementation class ITMassRegisterBean
 */
@Stateful
@Remote(ITMassRegister.class)
//@RemoteBinding(jndiBinding = "octopus/mcd/itmass/register")
@EJB(name = "octopus/mcd/itmass/register", beanInterface = ITMassRegister.class) 
public class ITMassRegisterBean implements ITMassRegister {
	//@EJB //(mappedName="octopus/mcd/management/register")
	@Inject
	private MgmtRegister register;
	
	/**
     * @see OCTRegister#doRegister()
     */
    public void doRegister() {
    	// Registre tudo que quer se seja um JOB dentro deste project
		register.registerJob(IncidentGetterBean.class);
    }

}
